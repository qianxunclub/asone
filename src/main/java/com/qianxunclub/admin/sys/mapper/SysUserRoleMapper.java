package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysUserRole;
import com.qianxunclub.admin.sys.model.request.SysUserRoleParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysUserRoleMapper  extends IMapper<SysUserRole,SysUserRoleParam> {

}