package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysRoleResource;
import com.qianxunclub.admin.sys.model.request.SysRoleResourceParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysRoleResourceMapper extends IMapper<SysRoleResource,SysRoleResourceParam> {
}