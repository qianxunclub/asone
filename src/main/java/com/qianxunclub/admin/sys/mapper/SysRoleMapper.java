package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysRole;
import com.qianxunclub.admin.sys.model.request.SysRoleParam;
import com.qianxunclub.context.mapper.IMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper extends IMapper<SysRole,SysRoleParam> {

    List<SysRole> roleListByUser(@Param("param") SysRoleParam param);
}