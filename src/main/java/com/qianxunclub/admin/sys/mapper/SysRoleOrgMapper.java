package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysRoleOrg;
import com.qianxunclub.admin.sys.model.request.SysRoleOrgParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysRoleOrgMapper extends IMapper<SysRoleOrg,SysRoleOrgParam> {

}