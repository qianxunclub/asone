package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysResource;
import com.qianxunclub.admin.sys.model.request.SysResourceParam;
import com.qianxunclub.context.mapper.IMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysResourceMapper extends IMapper<SysResource,SysResourceParam> {
    List<SysResource> resourceListByRole(@Param("param") SysResourceParam param);
}