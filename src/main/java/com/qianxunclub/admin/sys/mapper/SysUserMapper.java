package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysUserMapper extends IMapper<SysUser,SysUserParam> {
}