package com.qianxunclub.admin.sys.mapper;


import com.qianxunclub.admin.sys.model.domain.SysOrg;
import com.qianxunclub.admin.sys.model.request.SysOrgParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysOrgMapper extends IMapper<SysOrg, SysOrgParam> {

}