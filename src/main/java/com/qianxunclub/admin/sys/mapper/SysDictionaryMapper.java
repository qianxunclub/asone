package com.qianxunclub.admin.sys.mapper;

import com.qianxunclub.admin.sys.model.domain.SysDictionary;
import com.qianxunclub.admin.sys.model.request.SysDictionaryParam;
import com.qianxunclub.context.mapper.IMapper;

public interface SysDictionaryMapper  extends IMapper<SysDictionary, SysDictionaryParam> {
}