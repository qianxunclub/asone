package com.qianxunclub.admin.sys.constant;

/**
 * Created by zhangbin on 2017/2/21.
 */
public class MassageConstant {

    private MassageConstant(){

    }

    /**
     *用户名或密码错误
     */
    public static final String LOGIN_USER_REEOE = "LOGIN_USER_REEOE";
    /**
     *用户被锁定
     */
    public static final String LOGIN_USER_LOCK = "LOGIN_USER_LOCK";
    /**
     *密码错误次数已达上限
     */
    public static final String LOGIN_USER_MORE = "LOGIN_USER_MORE";

}
