package com.qianxunclub.admin.sys.constant;

/**
 * Created by zhangbin on 2017/2/21.
 */
public class ValidateConstant {

    private ValidateConstant(){
    }

    /**
     *
     */
    public static final String LOGIN_USERNAME_NOT_NULL = "用户名或密码不能为空";

}
