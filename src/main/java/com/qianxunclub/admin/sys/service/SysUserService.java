package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.mapper.SysUserMapper;
import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.context.service.AbstractService;
import com.qianxunclub.util.BaseResponse;
import com.qianxunclub.util.ReturnCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zhangbin on 2017/2/21.
 */
@Component
public class SysUserService extends AbstractService<SysUser,SysUserParam,SysUserMapper> {

    @Autowired
    private SysUserMapper sysUserMapper;

    public BaseResponse<SysUser> getByUsername(String username){
        BaseResponse<SysUser> baseResponse = new BaseResponse<SysUser>();
        SysUserParam param = new SysUserParam();
        param.setUsername(username);
        List<SysUser> list = sysUserMapper.selectBy(param);
        if(list.size() <= 0){
            baseResponse.setReturn(ReturnCode.CODE_1003);
        } else {
            baseResponse.setDataInfo(list.get(0));
        }
        return baseResponse;
    }

}
