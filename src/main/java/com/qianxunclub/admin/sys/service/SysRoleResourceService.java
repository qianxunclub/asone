package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.mapper.SysRoleResourceMapper;
import com.qianxunclub.admin.sys.model.domain.SysRoleResource;
import com.qianxunclub.admin.sys.model.request.SysRoleResourceParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

@Component
public class SysRoleResourceService extends AbstractService<SysRoleResource,SysRoleResourceParam,SysRoleResourceMapper> {
}