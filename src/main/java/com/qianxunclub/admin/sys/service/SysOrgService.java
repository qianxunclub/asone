package com.qianxunclub.admin.sys.service;


import com.qianxunclub.admin.sys.mapper.SysOrgMapper;
import com.qianxunclub.admin.sys.model.domain.SysOrg;
import com.qianxunclub.admin.sys.model.request.SysOrgParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

@Component
public class SysOrgService extends AbstractService<SysOrg,SysOrgParam,SysOrgMapper> {
}