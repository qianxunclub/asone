package com.qianxunclub.admin.sys.service;


import com.qianxunclub.admin.sys.constant.SysConstant;
import com.qianxunclub.admin.sys.mapper.SysResourceMapper;
import com.qianxunclub.admin.sys.model.domain.SysResource;
import com.qianxunclub.admin.sys.model.domain.SysRole;
import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.model.request.SysResourceParam;
import com.qianxunclub.admin.sys.model.request.SysRoleParam;
import com.qianxunclub.context.service.AbstractService;
import com.qianxunclub.redis.RedisCatch;
import com.qianxunclub.util.JsonUtil;
import com.qianxunclub.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SysResourceService extends AbstractService<SysResource,SysResourceParam,SysResourceMapper> {

    @Autowired
    private SysRoleService sysRoleService;

    public  List<SysResource> resourceListByRole(SysResourceParam param){
        return this.mapper.resourceListByRole(param);
    }
    public List<SysResource> getMenu(SysUser sysUser){
        Object redisMenu = RedisCatch.get(SysConstant.MENU + sysUser.getId());
        List<SysResource> menu = new ArrayList<SysResource>();
        if(redisMenu != null){
            menu = JsonUtil.jsonToObject(redisMenu.toString(),List.class);
        }
        if(menu == null || menu.size() == 0){
            List<SysResource> sysResourceList = this.getAllResourceByUser(sysUser);
            List<SysResource> root = this.getRootResource(sysResourceList);
            for(SysResource sysResource : root){
                List<SysResource> childResource = this.childResource(sysResourceList,sysResource);
                sysResource.setChildResource(childResource);
                menu.add(sysResource);
            }
            RedisCatch.put(SysConstant.MENU + sysUser.getId(), JsonUtil.objectToJson(menu));
        }
        return menu;
    }

    public List<SysResource> getAllResourceByUser(SysUser sysUser){
        List<String> roleList = new ArrayList<String>();
        SysRoleParam sysRoleParam = new SysRoleParam();
        sysRoleParam.setUserId(sysUser.getId());
        sysRoleParam.setOrgId(sysUser.getOrgId());
        List<SysRole> sysRoles = sysRoleService.roleListByUser(sysRoleParam);
        for(SysRole sysRole : sysRoles){
            roleList.add(sysRole.getId());
        }
        SysResourceParam param = new SysResourceParam();
        param.setRoleList(roleList);
        List<SysResource> sysResourceList = this.resourceListByRole(param);
        return sysResourceList;
    }

    public List<SysResource> getRootResource(List<SysResource> sysResourceList){
        List<SysResource> root = new ArrayList<SysResource>();
        for (SysResource sysResource : sysResourceList){
            if(StringUtil.isEmpty(sysResource.getParentId())){
                root.add(sysResource);
            }
        }
        return root;
    }

    private List<SysResource> childResource(List<SysResource> sysResourceList,SysResource target){
        String id = target.getId();
        List<SysResource> resources = new ArrayList<SysResource>();
        for (SysResource sysResource : sysResourceList){
            if(sysResource.getParentId().equals(id)){
                List<SysResource> childResource = this.childResource(sysResourceList,sysResource);
                sysResource.setChildResource(childResource);
                resources.add(sysResource);
            }
        }
        return resources;
    }
}