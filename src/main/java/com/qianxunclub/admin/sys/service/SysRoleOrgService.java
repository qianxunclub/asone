package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.mapper.SysRoleOrgMapper;
import com.qianxunclub.admin.sys.model.domain.SysRoleOrg;
import com.qianxunclub.admin.sys.model.request.SysRoleOrgParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

@Component
public class SysRoleOrgService extends AbstractService<SysRoleOrg,SysRoleOrgParam,SysRoleOrgMapper> {
}