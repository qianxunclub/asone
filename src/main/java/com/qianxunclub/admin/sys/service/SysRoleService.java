package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.mapper.SysRoleMapper;
import com.qianxunclub.admin.sys.model.domain.SysRole;
import com.qianxunclub.admin.sys.model.request.SysRoleParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SysRoleService extends AbstractService<SysRole,SysRoleParam,SysRoleMapper> {


    public List<SysRole> roleListByUser(SysRoleParam param){
        return this.mapper.roleListByUser(param);
    }
}