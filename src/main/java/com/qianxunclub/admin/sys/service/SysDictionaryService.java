package com.qianxunclub.admin.sys.service;


import com.qianxunclub.admin.sys.mapper.SysDictionaryMapper;
import com.qianxunclub.admin.sys.model.domain.SysDictionary;
import com.qianxunclub.admin.sys.model.request.SysDictionaryParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

@Component
public class SysDictionaryService extends AbstractService<SysDictionary,SysDictionaryParam,SysDictionaryMapper> {
}