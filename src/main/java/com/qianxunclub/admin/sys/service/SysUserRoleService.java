package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.mapper.SysUserRoleMapper;
import com.qianxunclub.admin.sys.model.domain.SysUserRole;
import com.qianxunclub.admin.sys.model.request.SysUserRoleParam;
import com.qianxunclub.context.service.AbstractService;
import org.springframework.stereotype.Component;

@Component
public class SysUserRoleService extends AbstractService<SysUserRole,SysUserRoleParam,SysUserRoleMapper> {
}