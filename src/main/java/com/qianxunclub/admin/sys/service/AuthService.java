package com.qianxunclub.admin.sys.service;

import com.qianxunclub.admin.sys.constant.MassageConstant;
import com.qianxunclub.admin.sys.constant.SysConstant;
import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.context.service.MessageService;
import com.qianxunclub.util.BaseResponse;
import com.qianxunclub.util.ReturnCode;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by zhangbin on 2017/3/10.
 */
@Component
public class AuthService {

    private Logger logger = Logger.getLogger(getClass());

    @Autowired
    private MessageService messageService;

    public BaseResponse login(HttpServletRequest request,SysUserParam param){
        BaseResponse baseResponse;
        baseResponse = this.validateUser(param);
        if(!baseResponse.isSuccess()){
            return baseResponse;
        }
        SysUser sysUser = (SysUser)request.getSession().getAttribute(SysConstant.USER_SESSION);
        baseResponse.setDataInfo(sysUser);
        return baseResponse;
    }

    public void logout() {
        SecurityUtils.getSubject().logout();
    }

    public BaseResponse isLogin() {
        BaseResponse baseResponse = new BaseResponse();
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isAuthenticated()) {
            baseResponse.setReturn(ReturnCode.CODE_1008);
        } else {
            baseResponse.setReturn(ReturnCode.CODE_1000);
        }
        return baseResponse;
    }

    private BaseResponse validateUser(SysUserParam param){
        BaseResponse baseResponse = new BaseResponse();
        String message = null;
        Integer code = ReturnCode.CODE_1000.code();
        String username = param.getUsername();
        String password = param.getPassword();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(param.isRememberMe());
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
        } catch (UnknownAccountException uae) {
            message = messageService.message(MassageConstant.LOGIN_USER_REEOE);
            logger.error(message,uae);
        } catch (IncorrectCredentialsException ice) {
            message = messageService.message(MassageConstant.LOGIN_USER_REEOE);
            logger.error(message,ice);
        } catch (LockedAccountException lae) {
            message = messageService.message(MassageConstant.LOGIN_USER_LOCK);
            logger.error(message,lae);
        } catch (ExcessiveAttemptsException eae) {
            message = messageService.message(MassageConstant.LOGIN_USER_MORE);
            logger.error(message,eae);
        } catch (AuthenticationException ae) {
            //通过处理Shiro的运行时AuthenticationException就可以控制用户登录失败或密码错误时的情景
            message = messageService.message(MassageConstant.LOGIN_USER_REEOE);
            logger.error(message,ae);
        }
        //验证是否登录成功
        if (!subject.isAuthenticated()) {
            code = ReturnCode.CODE_1006.code();
            token.clear();
        }
        baseResponse.setReturnCode(code);
        baseResponse.setMessage(message);
        return baseResponse;
    }


}
