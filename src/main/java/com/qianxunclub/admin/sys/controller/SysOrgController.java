package com.qianxunclub.admin.sys.controller;


import com.qianxunclub.admin.sys.model.domain.SysOrg;
import com.qianxunclub.admin.sys.model.request.SysOrgParam;
import com.qianxunclub.admin.sys.service.SysOrgService;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.PageDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/org")
public class SysOrgController {

    @Autowired
    private SysOrgService sysOrgService;

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void list(HttpServletResponse resp,@RequestBody SysOrgParam param) {
        PageDataResponse<SysOrg> pageDataResponse = sysOrgService.list(param);
        HttpResponseUtil.setResponseJsonBody(resp, pageDataResponse);
    }
}
