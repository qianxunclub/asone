package com.qianxunclub.admin.sys.controller;


import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.admin.sys.service.AuthService;
import com.qianxunclub.util.BaseResponse;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.ReturnCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/auth")
public class AuthController {


    @Autowired
    private AuthService authService;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(HttpServletRequest request, HttpServletResponse resp, @RequestBody SysUserParam param) {
        BaseResponse baseResponse = param.validate();
        if(baseResponse.isSuccess()){
            baseResponse = authService.login(request,param);
        }
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }

    @ResponseBody
    @RequestMapping(value = "/isLogin", method = {RequestMethod.POST,RequestMethod.GET})
    public void isLogin(HttpServletResponse resp) {
        BaseResponse baseResponse = authService.isLogin();
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }

    @ResponseBody
    @RequestMapping(value = "/relogin", method = {RequestMethod.POST,RequestMethod.GET})
    public void relogin(HttpServletResponse resp) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setReturn(ReturnCode.CODE_1008);
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }

    @ResponseBody
    @RequestMapping(value = "/refused", method = {RequestMethod.POST,RequestMethod.GET})
    public void refused(HttpServletResponse resp) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setReturn(ReturnCode.CODE_1001);
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = {RequestMethod.POST,RequestMethod.GET})
    public void logout(HttpServletResponse resp) {
        BaseResponse baseResponse = new BaseResponse();
        authService.logout();
        baseResponse.setReturn(ReturnCode.CODE_1000);
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }

}
