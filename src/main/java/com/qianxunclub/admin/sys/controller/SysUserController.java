package com.qianxunclub.admin.sys.controller;


import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.admin.sys.service.SysUserService;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.PageDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/user")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void list(HttpServletResponse resp,@RequestBody SysUserParam param) {
        PageDataResponse<SysUser> pageDataResponse = sysUserService.list(param);
        HttpResponseUtil.setResponseJsonBody(resp, pageDataResponse);
    }
}
