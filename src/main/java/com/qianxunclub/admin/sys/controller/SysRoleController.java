package com.qianxunclub.admin.sys.controller;


import com.qianxunclub.admin.sys.model.domain.SysRole;
import com.qianxunclub.admin.sys.model.request.SysRoleParam;
import com.qianxunclub.admin.sys.service.SysRoleService;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.PageDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void list(HttpServletResponse resp,@RequestBody SysRoleParam param) {
        PageDataResponse<SysRole> pageDataResponse = sysRoleService.list(param);
        HttpResponseUtil.setResponseJsonBody(resp, pageDataResponse);
    }
}
