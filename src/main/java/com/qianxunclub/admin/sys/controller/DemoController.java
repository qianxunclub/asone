package com.qianxunclub.admin.sys.controller;

import com.qianxunclub.admin.sys.model.request.SysUserParam;
import com.qianxunclub.util.BaseResponse;
import com.qianxunclub.util.HttpResponseUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zhangbin on 2017/3/21.
 */

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/demo")
public class DemoController {

    @ResponseBody
    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public void demo(HttpServletRequest request, HttpServletResponse resp, @RequestBody SysUserParam param) {
        BaseResponse baseResponse = param.validate();
        if(baseResponse.isSuccess()){

        }
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }
}
