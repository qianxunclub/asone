package com.qianxunclub.admin.sys.controller;


import com.qianxunclub.admin.sys.model.domain.SysDictionary;
import com.qianxunclub.admin.sys.model.request.SysDictionaryParam;
import com.qianxunclub.admin.sys.service.SysDictionaryService;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.PageDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/dictionary")
public class SysDictionaryController {

    @Autowired
    private SysDictionaryService sysDictionaryService;

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void list(HttpServletResponse resp,@RequestBody SysDictionaryParam param) {
        PageDataResponse<SysDictionary> pageDataResponse = sysDictionaryService.list(param);
        HttpResponseUtil.setResponseJsonBody(resp, pageDataResponse);
    }
}
