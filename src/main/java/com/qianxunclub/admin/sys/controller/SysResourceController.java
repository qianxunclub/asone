package com.qianxunclub.admin.sys.controller;

import com.qianxunclub.admin.sys.constant.SysConstant;
import com.qianxunclub.admin.sys.model.domain.SysResource;
import com.qianxunclub.admin.sys.model.domain.SysUser;
import com.qianxunclub.admin.sys.service.SysResourceService;
import com.qianxunclub.util.HttpResponseUtil;
import com.qianxunclub.util.PageDataResponse;
import com.qianxunclub.util.ReturnCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by zhangbin on 2017/3/21.
 */

@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping(value = "/sys/resource")
public class SysResourceController {

    @Autowired
    private SysResourceService sysResourceService;

    @ResponseBody
    @RequestMapping(value = "/menu", method = RequestMethod.POST)
    public void menu(HttpServletRequest request, HttpServletResponse resp) {
        PageDataResponse<SysResource> baseResponse = new PageDataResponse();
        SysUser sysUser = (SysUser)request.getSession().getAttribute(SysConstant.USER_SESSION);
        if(sysUser == null){
            baseResponse.setReturn(ReturnCode.CODE_1008);
            HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
            return;
        }
        List<SysResource> menu = sysResourceService.getMenu(sysUser);
        baseResponse.setPageData(menu);
        HttpResponseUtil.setResponseJsonBody(resp, baseResponse);
    }
}
