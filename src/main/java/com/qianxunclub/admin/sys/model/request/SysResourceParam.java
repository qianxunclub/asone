package com.qianxunclub.admin.sys.model.request;

import com.qianxunclub.context.Model.BaseModel;

import java.util.List;

public class SysResourceParam extends BaseModel<SysResourceParam> {
    private List<String> roleList;

    public List<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }
}