package com.qianxunclub.admin.sys.model.request;

import com.qianxunclub.admin.sys.constant.ValidateConstant;
import com.qianxunclub.context.Model.BaseModel;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * Created by zhangbin on 2017/2/21.
 */
public class SysUserParam extends BaseModel<SysUserParam> {
    private String id;
    @NotEmpty(message = ValidateConstant.LOGIN_USERNAME_NOT_NULL)
    private String username;
    @NotEmpty(message = ValidateConstant.LOGIN_USERNAME_NOT_NULL)
    private String password;
    private boolean rememberMe = false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
