package com.qianxunclub.admin.sys.model.request;

import com.qianxunclub.context.Model.BaseModel;

public class SysUserRoleParam  extends BaseModel {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}