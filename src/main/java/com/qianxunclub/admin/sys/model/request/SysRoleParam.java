package com.qianxunclub.admin.sys.model.request;


import com.qianxunclub.context.Model.BaseModel;

public class SysRoleParam  extends BaseModel<SysRoleParam> {
    private String userId;
    private String orgId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}