package com.qianxunclub.admin.sys.model.request;


import com.qianxunclub.context.Model.BaseModel;

public class SysOrgParam extends BaseModel<SysOrgParam> {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}